# Shapley Lectures :sunglasses:
Jupyter Notebooks for the Shapley lectures during the [SINTEF Geilo Winter School 2021](https://www.sintef.no/projectweb/geilowinterschool/2021/). All notebooks run standalone (no relative imports). Needless to say, keep your hands out of the Solutions directory as long as you still want to solve the tasks from the lectures on your own.   
  
Slides will be published here after the School.

# Requirements
- `Python 3` 

Available with pip:
- `shap`
- `keras`
- `tensorflow==2.3.1` or lower
- `xgboost`
- `sklearn`
- `dcor`
- `pandas`
- `numpy`
- `matplotlib`
- `seaborn`
- `kumaraswamy` (optional)

# Useful resources
- [The Handbook of the Shapley Value](https://www.researchgate.net/publication/337935755_Handbook_of_the_Shapley_Value)
- [Slundberg SHAP git-repo](https://github.com/slundberg/shap)
- [Explaining the data or explaining a model? Shapley values that uncover non-linear dependencies](https://arxiv.org/pdf/2007.06011v1.pdf)
- [Explaining individual predictions when features are dependent: More accurate approximations to Shapley values](https://arxiv.org/abs/1903.10464v1)
